package com.pack;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        ArrayList<Student> arr = new ArrayList<Student>();
        arr.add(new Student(1,27,"Oleg"));
        arr.add(new Student(2,24,"Ivan"));
        arr.add(new Student(3,51,"Ivan"));
        arr.add(new Student(4,33,"Gregory"));


        System.out.println(sumByName(arr, "Ivan"));

        System.out.println(nameSet(arr));

        System.out.println(containStudentByAge(arr,40));

        System.out.println(toMapById(arr));

        printmap(toMapByAge(arr));
    }
    private static void printmap(Map<Integer,Stream> map){
        for (Map.Entry<Integer, Stream> pair : map.entrySet()) {
            Integer key = pair.getKey();
            System.out.println(key + ":");
            pair.getValue().forEach(System.out::println);
        }
    }

    private static int sumByName(ArrayList<Student> arr, String name){
       return arr.stream().filter((s) -> Objects.equals(s.getName(), name)).mapToInt(Student::getAge).sum();
    }
    private static Set<String> nameSet(ArrayList<Student> arr){
        return arr.stream().map(Student::getName).collect(Collectors.toSet());
    }
    private static boolean containStudentByAge(ArrayList<Student> arr, int age){
        return arr.stream().anyMatch((s) -> s.getAge() > age);
    }
    private static Map<Integer,String> toMapById(ArrayList<Student> arr){
        return arr.stream().collect(Collectors.toMap(
                Student::getId,
                Student::getName));
    }
    private static Map<Integer,Stream> toMapByAge(ArrayList<Student> arr){
        return arr.stream().collect(Collectors.toMap(
                Student::getAge,(s) -> arr.stream().filter((p) -> p.getAge() == s.getAge())));
    }
}
